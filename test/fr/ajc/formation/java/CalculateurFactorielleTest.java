package fr.ajc.formation.java;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalculateurFactorielleTest {

	@Test
	void factorielle_de_0_should_be_1() {
		assertEquals(1, CalculateurFactorielle.factorielle(0));
	}
	@Test
	void factorielle_de_1_should_be_1() {
		assertEquals(1, CalculateurFactorielle.factorielle(1));
	}
	@Test
	void factorielle_de_3_should_be_6() {
		assertEquals(1, CalculateurFactorielle.factorielle(3));
	}
	@Test
	void factorielle_de_6_should_be_720() {
		assertEquals(1, CalculateurFactorielle.factorielle(6));
	}
}
