package fr.ajc.formation.java;
public class CalculateurFactorielle	{
	public static long Factorielle(long fact) {
        if (fact <= 1)
            return 1;
        else
            return fact * Factorielle(fact - 1);
    }

	
//		else if (entier > 1) {
//			long i = 2;
//			long fin = entier;
//			long resultat = 1;
//			while (i != fin) {
//				resultat = resultat * i;
//				i = i + 1;
//			}
//			return resultat * entier;
//		} else {
//			return 0;
//	}
	
	public static void main(String[] args) {
		for (int counter = 0; counter <= 6; counter++){
	        System.out.printf("%d! = %d\n", counter,
	        Factorielle(counter));
	}
}
}
